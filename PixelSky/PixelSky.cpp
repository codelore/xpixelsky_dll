// PixelSky.cpp: ���������� ���������������� ������� ��� ���������� DLL.
//

#include "stdafx.h"
#include <jni.h>
#include <iostream>
#include "ru_pixelsky_xprotect_XProtect.h"
#include <stdio.h>  
#include <stdlib.h> 
#pragma warning(disable:4996)
//#pragma warning(disable:4700)
#define _WIN32_WINNT 0x0400

#include <windows.h>


	JNIEXPORT jstring JNICALL Java_ru_pixelsky_xprotect_XProtect_getHwid(JNIEnv* env, jclass myclass)
    {
		jstring s;
		
        HW_PROFILE_INFO hwProfileInfo;

		if(GetCurrentHwProfile(&hwProfileInfo) != NULL)
		{
			wchar_t *str = hwProfileInfo.szHwProfileGuid;
			size_t len = wcslen(str);  
			if (sizeof(wchar_t) != sizeof(jchar)) //C����� � Linux
			{  
				jchar* str2 = (jchar*)malloc((len+1)*sizeof(jchar));  
				int i;  
				for (i = 0; i < len; i++)   
				str2[i] = str[i];  
				str2[len] = 0;  
				s = env->NewString(str2, len);    
				free(str2);  
				return s;  
			}  
			else // Else Windows
			{
				const size_t len = wcslen(str) +1;
				char *buf = new char[len]();
				WideCharToMultiByte(CP_ACP,0,str,-1,buf,len,NULL,NULL);
				s = env -> NewStringUTF(buf);
				return s;
			}
		}
	}

	JNIEXPORT jobjectArray JNICALL Java_ru_pixelsky_xprotect_XProtect_checkedDirs(JNIEnv *env, jclass myclass)
    {
		jobjectArray ret;

		int i, dirsCount = 0;

		char *data[3]= {"bin", "mods", "coremods"};

		dirsCount = sizeof(data) / sizeof(*data);

		ret= (jobjectArray)env->NewObjectArray(dirsCount,env->FindClass("java/lang/String"),env->NewStringUTF(""));

		for(i=0;i<dirsCount;i++)
		{
			env->SetObjectArrayElement(ret,i,env->NewStringUTF(data[i]));
		}

		return ret;
    }
